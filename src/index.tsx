import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './components/App/App';
import SurveyProvider from './providers/SurveyProvider/SurveyProvider';
import './styles/tailwind.css';

ReactDOM.render(
  <BrowserRouter>
    <SurveyProvider>
      <App />
    </SurveyProvider>
  </BrowserRouter>,
  document.getElementById('survey')
);
