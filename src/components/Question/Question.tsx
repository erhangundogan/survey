import React from 'react';

const Question = ({ text }) => {
  return (
    <div className="question">
      <h1 className="mt-5 mb-5 text-2xl font-medium text-black dark:text-gray-300">{text}</h1>
    </div>
  );
};

export default Question;
