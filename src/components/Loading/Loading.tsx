import React from 'react';
import './Loading.css';

const Loading = () => {
  return (
    <div className="loading-container active">
      <h4>Loading, please wait.</h4>
      <div className="loading" />
    </div>
  );
};

export default Loading;
