import React, { useEffect, useState } from 'react';
import { Redirect, useHistory, useParams } from 'react-router-dom';
import { useSurvey } from '../../providers/SurveyProvider/SurveyProvider';
import { SurveyResultType, SurveyType } from '../../providers/SurveyProvider/SurveyProvider.types';
import Loading from '../Loading/Loading';
import CompletedSurvey from '../CompletedSurvey/CompletedSurvey';
import NewSurvey from '../NewSurvey/NewSurvey';

const Survey = () => {
  const history = useHistory();
  const [loading, setLoading] = useState<boolean>(true);
  const { list, completedList } = useSurvey();
  const { surveyId } = useParams<{ surveyId: string }>();
  const [survey, setSurvey] = useState<SurveyType>();
  const [result, setResult] = useState<SurveyResultType>();

  useEffect(() => {
    const surveyItem = list?.find(({ id }) => id === surveyId);
    const completedSurveyItem = completedList?.find(({ surveyId: id }) => id === surveyId);

    if (surveyItem) {
      setSurvey(surveyItem);
    } else {
      return history.push('/');
    }

    if (completedSurveyItem) {
      setResult(completedSurveyItem);
    }
    setLoading(false);
  }, [surveyId, list, completedList]);

  if (!surveyId) {
    return <Redirect to="/" />;
  }

  if (loading) {
    return <Loading />;
  }

  if (result && survey) {
    return <CompletedSurvey result={result} survey={survey} />;
  }

  if (survey) {
    return <NewSurvey item={survey} startedAt={Date.now()} />;
  }

  return null;
};

export default Survey;
