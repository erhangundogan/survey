import React from 'react';
import { Link } from 'react-router-dom';

const CompletedSurvey = ({ result, survey }) => {
  const { startedAt, finishedAt, scores } = result;
  const { title } = survey;

  return (
    <div className="min-h-screen py-6 flex flex-col justify-center sm:py-12">
      <div className="relative py-3 max-w-full mx-auto">
        <div className="absolute inset-0 bg-gradient-to-r from-blue-500 to-purple-400 shadow-lg transform skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl" />
        <div className="relative px-4 py-10 bg-white dark:bg-gray-800 shadow-lg sm:rounded-3xl sm:p-10">
          <div className="mx-auto">
            <div className="divide-y divide-gray-100">
              <h1 className="mt-4 mb-2 text-2xl font-medium text-black dark:text-gray-300">{title}</h1>
            </div>
            <div className="divide-y divide-gray-100">
              <h3 className="mb-4 text-sm font-medium text-gray-700 dark:text-gray-500">
                Duration: {(finishedAt - startedAt) / 1000} secs
              </h3>
            </div>
            {scores ? (
              <div>
                <h3 className="mt-2 mb-2 text-xl font-medium text-gray-700 dark:text-gray-400 border-b-2 border-gray-400">
                  Scores:
                </h3>
                <ul className="space-y-2 mb-10">
                  <li>Extraversion: {scores['Extraversion']}</li>
                  <li>Agreeableness: {scores['Agreeableness']}</li>
                  <li>Conscientiousness: {scores['Conscientiousness']}</li>
                  <li>Emotional Stability: {scores['Emotional Stability']}</li>
                  <li>Intellect/Imagination: {scores['Intellect/Imagination']}</li>
                </ul>
                <Link
                  to="/"
                  className="py-2 px-4 bg-blue-500 text-white font-semibold rounded-lg shadow-md hover:bg-blue-700 whitespace-nowrap"
                >
                  Go back to home page
                </Link>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CompletedSurvey;
