import React from 'react';
import { render } from '@testing-library/react';
import CompletedSurvey from '../CompletedSurvey';
import surveys from '../../../providers/SurveyProvider/data/surveys.json';

jest.mock('react-router-dom', () => ({
  // @ts-ignore
  ...jest.requireActual('react-router-dom'),
  Link: jest.fn(() => <p>Link</p>)
}));

describe('CompletedSurvey', () => {
  const result = {
    surveyId: '672deb0c-32b2-49a2-9ffb-9b8619a68389',
    answers: [],
    startedAt: Date.now() - 20000,
    finishedAt: Date.now(),
    scores: {
      Extraversion: 1,
      Agreeableness: 2,
      Conscientiousness: 3,
      'Emotional Stability': 4,
      'Intellect/Imagination': 5
    }
  };
  const completedSurvey = () => render(<CompletedSurvey survey={surveys[0]} result={result} />);

  test('renders', () => {
    const { asFragment } = completedSurvey();
    expect(asFragment()).toMatchSnapshot();
  });
});
