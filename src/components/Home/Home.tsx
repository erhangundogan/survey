import React from 'react';
import SurveyList from '../SurveyList/SurveyList';

const Home = () => {
  return (
    <div className="m-4 p-4">
      <h1 className="text-xl font-medium text-black dark:text-gray-300">Welcome to survey app</h1>
      <p className="text-gray-500">Please choose one of the surveys below to start</p>
      <div className="divide-y divide-gray-100">
        <SurveyList />
      </div>
    </div>
  );
};

export default Home;
