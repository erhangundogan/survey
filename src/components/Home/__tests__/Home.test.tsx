import React from 'react';
import { render } from '@testing-library/react';
import SurveyList from '../../SurveyList/SurveyList';
import Home from '../Home';

jest.mock('../../SurveyList/SurveyList');

describe('Home', () => {
  const home = () => render(<Home />);

  test('renders', () => {
    (SurveyList as jest.Mock).mockImplementationOnce(() => <div>SurveyList</div>);
    const { asFragment } = home();
    expect(asFragment()).toMatchSnapshot();
  });
});
