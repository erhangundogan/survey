import React from 'react';
import { Link } from 'react-router-dom';

const SurveyListItem = ({ item, completed, deleteSurveys }) => {
  return (
    <div className="p-4 m-4 max-w mx-auto bg-white dark:bg-gray-700 rounded-xl shadow-md flex flex-col space-x-4">
      <article className="p-2 md:p-4 flex flex-col md:flex-row">
        <div className="min-w-0 relative flex-auto">
          <h2 className="text-lg font-semibold text-black dark:text-gray-300 mb-0.5">
            <div className="flex flex-row">
              {item.title}
              {completed ? (
                <svg
                  className="flex-shrink-0 h-5 ml-1 w-5 text-green-400 hidden md:flex"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"
                  />
                </svg>
              ) : null}
            </div>
          </h2>
          <dl className="flex flex-wrap text-sm font-medium whitespace-pre">
            <div>
              <dt className="sr-only">Questions</dt>
              <dd>{item.questions.length} questions</dd>
            </div>
          </dl>
        </div>
        <div className="mt-4">
          {completed ? (
            <>
              <button
                onClick={deleteSurveys}
                className="py-1.5 px-4 mr-0.5 md:mr-2 bg-red-400 text-white rounded-lg shadow-md hover:bg-red-700 whitespace-nowrap"
              >
                Delete result
              </button>
              <Link
                to={`/survey/${item.id}`}
                className="py-2 px-4 bg-green-600 text-white font-semibold rounded-lg shadow-md hover:bg-green-800 whitespace-nowrap"
              >
                Show survey result
              </Link>
            </>
          ) : (
            <Link
              to={`/survey/${item.id}`}
              className="py-2 px-4 bg-blue-500 text-white font-semibold rounded-lg shadow-md hover:bg-blue-700 whitespace-nowrap"
            >
              Take me to the survey
            </Link>
          )}
        </div>
      </article>
    </div>
  );
};

export default SurveyListItem;
