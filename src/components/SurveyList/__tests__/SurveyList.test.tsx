import React from 'react';
import { render } from '@testing-library/react';
import SurveyList from '../SurveyList';
import SurveyProvider from '../../../providers/SurveyProvider/SurveyProvider';
import { MemoryRouter } from 'react-router-dom';

describe('SurveyList', () => {
  const surveyList = () =>
    render(
      <MemoryRouter>
        <SurveyProvider>
          <SurveyList />
        </SurveyProvider>
      </MemoryRouter>
    );

  test('renders', () => {
    const { asFragment } = surveyList();
    expect(asFragment()).toMatchSnapshot();
  });
});
