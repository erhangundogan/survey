import React, { useMemo } from 'react';
import SurveyListItem from '../SurveyListItem/SurveyListItem';
import { useSurvey } from '../../providers/SurveyProvider/SurveyProvider';

const SurveyList = () => {
  const { list, completedList, deleteSurveys } = useSurvey();

  const completedListIds = useMemo(() => completedList.map(({ surveyId }) => surveyId), [completedList]);

  return (
    <ul>
      {list.map((survey) => {
        const completed = completedListIds.includes(survey.id);
        return (
          <SurveyListItem key={survey.id} item={survey} completed={completed} deleteSurveys={deleteSurveys} />
        );
      })}
    </ul>
  );
};

export default SurveyList;
