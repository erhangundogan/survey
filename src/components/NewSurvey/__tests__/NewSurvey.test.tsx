import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import NewSurvey from '../NewSurvey';
import surveys from '../../../providers/SurveyProvider/data/surveys.json';

describe('NewSurvey', () => {
  const time = Date.now();
  const newSurvey = () => render(<NewSurvey item={surveys[0]} startedAt={time} />);

  test('renders', () => {
    const { asFragment } = newSurvey();
    expect(asFragment()).toMatchSnapshot();
  });

  test('cannot proceed without selecting answer', () => {
    const { getByTestId } = newSurvey();
    const nextButton = getByTestId('next');
    expect(nextButton).toBeInTheDocument();
    expect(nextButton).toHaveAttribute('disabled');
  });

  test('proceeds after selecting answer', () => {
    const { getAllByRole, getByTestId } = newSurvey();
    const answers = getAllByRole('radio');
    fireEvent.click(answers[0]);
    const nextButton = getByTestId('next');
    expect(nextButton).not.toHaveAttribute('disabled');
  });
});
