import React, { useEffect, useState } from 'react';
import Question from '../Question/Question';
import Answers from '../Answers/Answers';
import { QuestionResultType } from '../../providers/SurveyProvider/SurveyProvider.types';
import { useSurvey } from '../../providers/SurveyProvider/SurveyProvider';
import calculateScores from '../../helpers/calculateScores';

const NewSurvey = ({ item: survey, startedAt }) => {
  const { setCompletedList } = useSurvey();
  const [questionIndex, setQuestionIndex] = useState(0);
  const [answers, setAnswers] = useState<Array<QuestionResultType>>([]);
  const currentQuestion = survey?.questions[questionIndex];

  useEffect(() => {
    if (Array.isArray(answers)) {
      if (survey.questions.length === answers.length) {
        const completedSurveyItem = {
          surveyId: survey.id,
          answers,
          startedAt,
          finishedAt: Date.now(),
          scores: calculateScores({ survey, answers })
        };
        setCompletedList((current) => [...current, completedSurveyItem]);
      }
    }
  }, [answers]);

  if (!currentQuestion) {
    return null;
  }

  const selectAnswer = (answerId) => {
    const selected = {
      questionId: currentQuestion.id,
      answerId
    };
    setAnswers((current) => [...current, selected]);
    setQuestionIndex(questionIndex + 1);
  };

  return (
    <div className="min-h-screen py-6 flex flex-col justify-center sm:py-12">
      <div className="relative py-3 max-w-full mx-auto">
        <div className="absolute inset-0 bg-gradient-to-r from-yellow-500 to-red-400 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl" />
        <div className="relative px-4 py-10 bg-white dark:bg-gray-800 shadow-lg sm:rounded-3xl sm:p-10">
          <div className="mx-auto">
            <div>
              <Question text={currentQuestion.question} />
            </div>
            <div className="divide-y divide-gray-200">
              <Answers selectAnswer={selectAnswer} questionIndex={questionIndex} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewSurvey;
