import React, { useRef, useState } from 'react';
import answers from '../../providers/SurveyProvider/data/answers.json';

const Answers = ({ selectAnswer, questionIndex }) => {
  const formRef = useRef();
  const [selected, setSelected] = useState<number | null>(null);

  const handleSubmit = (event) => {
    event.preventDefault();
    selectAnswer(selected);
    setSelected(null);
    (formRef.current as any).reset();
  };

  return (
    <div className="answers">
      <form onSubmit={handleSubmit} ref={formRef}>
        {answers.map(({ id, answer }) => {
          return (
            <div key={id} className="mb-5">
              <input
                type="radio"
                name="answer"
                id={`answer-${id}`}
                value={id}
                onClick={(event) => setSelected(+(event.target as any).value)}
                className="cursor-pointer py-2 px-2 m-2 text-white text-sm"
              />
              <label className="cursor-pointer" htmlFor={`answer-${id}`}>
                {answer}
              </label>
            </div>
          );
        })}
        <input
          data-testid="next"
          disabled={selected === null}
          type="submit"
          value="Next"
          className={`py-4 px-5 m-2 text-white rounded-lg ${
            selected !== null ? 'bg-blue-500 cursor-pointer' : 'bg-gray-400 cursor-not-allowed'
          }`}
        />
      </form>
    </div>
  );
};

export default Answers;
