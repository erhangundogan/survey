import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Home from '../Home/Home';
import Survey from '../Survey/Survey';

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/survey/:surveyId" component={Survey} />
      <Redirect to="/" />
    </Switch>
  );
};

export default App;
