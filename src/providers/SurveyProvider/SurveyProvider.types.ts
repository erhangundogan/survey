// https://ipip.ori.org/new_ipip-50-item-scale.htm
export type FactorType =
  | 'Extraversion'
  | 'Agreeableness'
  | 'Conscientiousness'
  | 'Emotional Stability'
  | 'Intellect/Imagination';

export type FactorEffect = 'Default' | 'Reverse';

export interface AnswerType {
  id: number;
  answer: string;
}
export interface QuestionType {
  id: number;
  question: string;
  answers?: AnswerType[];
  factor?: string | FactorType;
  effect?: string | FactorEffect;
}
export interface SurveyType {
  id: string;
  title: string;
  description?: string;
  questions: QuestionType[];
}
export interface QuestionResultType {
  questionId: string;
  answerId: string;
}
export interface SurveyResultType {
  surveyId: string;
  answers: QuestionResultType[];
  startAt?: number;
  finishedAt?: number;
  scores: any;
}
export interface SurveyContextType {
  list: SurveyType[];
  completedList?: SurveyResultType[];
  current?: SurveyResultType;
  setCurrent: any;
  setCompletedList: any;
  deleteSurveys: any;
}
