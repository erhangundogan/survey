import React, { createContext, useContext, useEffect, useState } from 'react';
import { SurveyContextType, SurveyResultType, SurveyType } from './SurveyProvider.types';
import surveys from './data/surveys.json';

const SurveyContext = createContext({} as SurveyContextType);
export const useSurvey = () => useContext(SurveyContext);

const SurveyProvider = ({ children }) => {
  const [list, setList] = useState<Array<SurveyType>>([]);
  const [completedList, setCompletedList] = useState<Array<SurveyResultType>>([]);
  const [current, setCurrent] = useState<SurveyResultType>();

  const deleteSurveys = () => {
    localStorage.clear();
    setCompletedList([]);
  };

  useEffect(() => {
    setList(surveys);

    try {
      const completed = localStorage.getItem('completed');
      if (completed) {
        setCompletedList(JSON.parse(completed));
      }
    } catch (error) {
      console.error('Could not parse previously saved results');
    }
  }, []);

  useEffect(() => {
    if (Array.isArray(completedList)) {
      localStorage.setItem('completed', JSON.stringify(completedList));
    }
  }, [completedList]);

  return (
    <SurveyContext.Provider
      value={{ list, completedList, current, setCurrent, setCompletedList, deleteSurveys }}
    >
      {children}
    </SurveyContext.Provider>
  );
};

export default SurveyProvider;
