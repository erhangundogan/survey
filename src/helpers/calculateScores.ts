const getScore = ({ answerId, effect }) => {
  const scores = [1, 2, 3, 4, 5];
  switch (effect) {
    case 'Default': {
      return scores[answerId];
    }
    case 'Reverse': {
      return scores.reverse()[answerId];
    }
  }
};

const calculateScores = ({ survey, answers }) => {
  const { questions } = survey;
  return answers.reduce(
    (acc, { answerId, questionId }) => {
      const { factor, effect } = questions.find(({ id }) => id === questionId);
      acc[factor] = (acc[factor] || 0) + getScore({ answerId, effect });
      return acc;
    },
    {
      Extraversion: 0,
      Agreeableness: 0,
      Conscientiousness: 0,
      'Emotional Stability': 0,
      'Intellect/Imagination': 0
    }
  );
};

export default calculateScores;
