import surveys from '../../providers/SurveyProvider/data/surveys.json';
import calculateScores from '../calculateScores';

describe('calculateScores', () => {
  test('calculates scores properly', () => {
    const answers = [
      { questionId: 1, answerId: 0 },
      { questionId: 2, answerId: 0 },
      { questionId: 3, answerId: 0 },
      { questionId: 4, answerId: 0 },
      { questionId: 5, answerId: 0 }
    ];
    const scores = calculateScores({ survey: surveys[0], answers });
    expect(scores).toEqual({
      Agreeableness: 1,
      Conscientiousness: 1,
      Extraversion: 5,
      'Emotional Stability': 5,
      'Intellect/Imagination': 1
    });
  });
});
