Survey
======

[![License][license-src]][license-href]

React survey client

- Provide survey/test journey
- Fetch surveys data from the API (Currently mock data from json file)
- Show the list of surveys
- Keep data of the completed surveys

Survey/test data taken from [International Personality Item Pool][resource-href]

## Demo

[Open app][app-href]

## Components

- Answers
- App
- CompletedSurvey
- Home
- Loading
- NewSurvey
- Question
- Survey
- SurveyList
- SurveyListItem

## Providers

- SurveyProvider

## Tech Stack

- React v17.0.2
- React Router v5.3.0
- TypeScript v4.5.2
- TailwindCSS v2.2.19
- WebPack v5.64.1

## Additional Features
- Dark theme
- Responsive design

## Setup/Run
```bash
git clone https://gitlab.com/erhangundogan/survey.git
cd survey
yarn
yarn start
```

## Test
```bash
yarn test
```

[license-src]: https://img.shields.io/badge/license-MIT-brightgreen.svg
[license-href]: LICENSE.md
[resource-href]: https://ipip.ori.org/new_ipip-50-item-scale.htm
[app-href]: https://survey-v1.vercel.app/
