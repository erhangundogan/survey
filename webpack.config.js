const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env, argv) => {
  const { mode = 'development' } = argv;
  const production = mode === 'production';

  const optimization = production ? {
    optimization: {
      minimize: true,
      minimizer: [
        new TerserPlugin(),
      ],
      splitChunks: {
        chunks: 'all',
      },
    }
  } : {};

  const config = {
    mode,
    entry: {
      index: path.join(__dirname, 'src', 'index.tsx')
    },
    target: 'web',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].js',
      clean: true
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.json', '.css']
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: 'public/index.html'
      }),
      ...production ? [new BundleAnalyzerPlugin({ generateStatsFile: false }) ] : []
    ],
    devtool: production ? false : 'inline-source-map',
    devServer: {
      static: {
        directory: path.join(__dirname, 'dist')
      },
      port: 9010,
      hot: true,
      historyApiFallback: {
        index: '/'
      },
      allowedHosts: 'all'
    },
    ...optimization,
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: '/node_modules/'
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader', 'postcss-loader'],
          exclude: /node_modules/,
        },
        {
          test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/i,
          loader: 'url-loader',
          options: {
            limit: 8192
          }
        }
      ],
    }
  };

  return config;
};
