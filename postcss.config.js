const purgecss = require('@fullhuman/postcss-purgecss');
const cssnano = require('cssnano');

const production = process.env.NODE_ENV === 'production';

const developmentConfig = {
  plugins: [
    require('postcss-preset-env'),
    require('postcss-import'),
    require('tailwindcss')
  ]
};
const productionConfig = {
  plugins: [
    require('postcss-preset-env'),
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
    cssnano({
      preset: 'default'
    }),
    purgecss({
      content: [
        './public/**/*.html',
        './src/**/*.tsx',
        './src/**/*.js'
      ],
      defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
    })
  ]
}

module.exports = production ? productionConfig : developmentConfig;
